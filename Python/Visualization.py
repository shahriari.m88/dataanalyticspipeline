import glob 
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from RollingAverage import passing_args, read_data



def plottingRollingAVG():

    print("The program entered here!")

    try:


        args = passing_args()

        print("printing the arguments!")

        print(args)

        # destructing the args
        outputpath = args[0]
        artifact_path = args[1]


        all_files = glob.glob(outputpath + "*.csv") 
        

        data = read_data(all_files)

        plt.figure(figsize=(12,6))

        plt.xticks(rotation=90)

        plt.xlabel('Dates', fontsize=15)

        plt.ylabel('Rolling AVG', fontsize=16)

        plt.title("45-day Rolling Average vs Days",  fontsize=18 , fontweight='bold')

        sns.lineplot(x=data['date'], y=data['rolling_avg'], data = data)

        plt.savefig(artifact_path + "RollingAverage.png", bbox_inches = "tight")

    except:
        print("Except")


if __name__ =='__main__':

    print("The program is running!")
    plottingRollingAVG()

    
else:
    print("The program is not running!")   