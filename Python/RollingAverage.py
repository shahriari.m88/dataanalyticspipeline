import os
import pandas as pd
import argparse
import re

'''
This is the module to calculate the Rolling Average

Possible Improvements:

1. Writing test cases
2. Further error handling of the chain
3. Improve passing of arguments
4. Check the input and output files before reading and writing of the files
5. Read and write from a cloud storage rather than a file system

'''


def read_data(file_paths):

    print("I am coming from the read data script")
    
    print(file_paths)

    li = []

    for file in  file_paths:

        df = pd.read_csv(file)
       
        li.append(df)
    
    frame = pd.concat(li, axis=0, ignore_index=True)
    print(file_paths)

   
    return frame

    
'''
Definition of the argparse and how to path the arguments

'''

def passing_args():

    # Receiving the input arguments required for the function
    parser = argparse.ArgumentParser(description='The script to perform analytics on NYC Taxi Data')
    # Define an argument list to pass all the inputs
    parser.add_argument('--list', '-d', nargs="+")
    args = parser.parse_args().list

    return args

'''

Function to create a new date column
Input: name of the input column as well as the column which needs to be created
Output: the dataframe with the new created columns

'''

def convert_date(df, source_column_name, target_column_name ):

    df[target_column_name] = pd.to_datetime(df[source_column_name].str[:10]) 

    return df   


'''
Function for filtering the dates
Input: It gets the data frame, the name of the column, start_date and end_date 
Output: The start_date and the end_date help to filter the unrelated data and the filtered dataframe is returned
'''
def filter_date(df,column_date, start_date,end_date):

    # Filtering the pandas dataframe based on the start_date and end_date
    mask = (df[column_date] > start_date) & (df[column_date] <= end_date)

    filtered_df=df.loc[mask]

    return filtered_df

'''
Function for performing analytics calculations
Input: In this case, it receives the dataframe and column_name
Output: It generates a dataframe which contains the average distance 

'''
def calculate_analytics(df, column_name, end_date):

    data = {

        'date':[end_date],
        'rolling_avg': [df[column_name].mean()]
    }

    print(data)
    
    output_df = pd.DataFrame(data, columns = ['date', 'rolling_avg'])

    return output_df

'''
The main() function which is the staring point of the program!
'''
def calculateRollingAVG():
    
    print("I am coming from the main!")

    try:
        
        args = passing_args()

        print("Here is the list of arguments")

        print(args)

        # Receiving the arguments from the bash
        nyc_data_path = args[0]
        prev_month_file_name = args[1]
        current_month_file_name = args[2]
        start_date = args[3]
        end_date = args[4]
        output_path = args[5]

        # Constructing the file path for the current and prev months

        file_paths = []
        file_paths.append(nyc_data_path + prev_month_file_name + '.csv')
        file_paths.append(nyc_data_path + current_month_file_name + '.csv')

        # Reading the data of prev and current months
        input_df = read_data(file_paths)
        # Adding a column as the date format
        df_data_column = convert_date(input_df, 'tpep_pickup_datetime', 'tpep_dropoff_date' )
        # Filtering the dataframe based on needed period
        df_filtered = filter_date(df_data_column,'tpep_dropoff_date',start_date, end_date)
        # Calculate the avg distance
        df_analytics_calculated = calculate_analytics(df_filtered,'trip_distance', end_date)

        print(df_analytics_calculated.head())
        # writing the data to the file
        df_analytics_calculated.to_csv(output_path + end_date + '.csv')

    except:
        print("There is something wrong to run the program")
        

if __name__ =='__main__':
    
    print("The program is running!")
    calculateRollingAVG()

    
else:
    print("The program is not running!") 