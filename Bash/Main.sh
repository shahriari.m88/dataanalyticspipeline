#This is the Main config of the program, and runs the RollingAverage.sh file
MASTER_CONFIG_FILE="$1"
MODE="$2" # Mode can be either "Today" or "Range"
if [ "$MASTER_CONFIG_FILE" == "" ]; then
        MASTER_CONFIG_FILE="D:\\Coding\\NYCTaxiDatasetPipeline\\RollingAveragePipeline\\CONFIG.sh"
        source $MASTER_CONFIG_FILE
else
    if [ -f "$MASTER_CONFIG_FILE" ]; then
    	echo $MASTER_CONFIG_FILE
        source $MASTER_CONFIG_FILE
    else
        echo "Config file $MASTER_CONFIG_FILE does not exist. Aborting execution ..."
        exit -1
    fi
fi
# Declaring empty variables!
unset START_DATE
unset END_DATE
# START_DATE of the run
START_DATE='2020-05-15' 
# END_DATE of the run
END_DATE='2020-05-25' 

if [ $MODE = "today" ]; then
echo "Setting the START_DATE and END_DATE to today"
START_DATE=$(date -d'today' +%Y%m%d)
END_DATE=$(date -d'today' +%Y%m%d)
else
# START_DATE of the run
START_DATE='2020-05-15' 
# END_DATE of the run
END_DATE='2020-06-25' 
fi         

# STARTED_DATE as the YMD format i.e., 20200515
START_DATE_YMD=$(date -d $START_DATE +%Y%m%d)
# END_DATE as the YMD format 
END_DATE_YMD=$(date -d $END_DATE +%Y%m%d)
# A helper variable to store the START_DATE
DELTA_DATE=$START_DATE
# Considering YMD format
DELTA_DATE_YMD=$START_DATE_YMD

while [[ $DELTA_DATE_YMD -le $END_DATE_YMD ]]

do
    echo "Calculating the rolling average for the week $DELTA_DATE"
    
    echo "START_DATE_YMD"
    echo "$START_DATE_YMD"
    echo "END_DATE_YMD"
    echo "$END_DATE_YMD"
    echo "DELTA_DATE"
    echo "$DELTA_DATE"

    DATE_TODAY=$DELTA_DATE
    # Running the RollingAverage Script with the updated dates
    source ../Bash/RollingAverage.sh 
    
    # Updating the DELTA_DATE_YMD
	DELTA_DATE_YMD=$(date -d "$DELTA_DATE_YMD +1 day" +%Y%m%d)
    # Updating the DELTA_DATE
    DELTA_DATE=$(date -d "$DELTA_DATE +1 day" +%Y-%m-%d)

done


echo "performing the visualization"
## Performing the visualization
python ${ROOT_PATH_CODE}\\python\\Visualization.py \
    --list "$OUTPUTPATH" \
           "$ARTIFACTS"