
#This bash file gets the DATE_TODAY variable, and compute the average distance for the last 45 days
#Possible improvements:

## 1. Year Change Improvement
## 2. Passing of arguments

## Generating the date arguments
ROLLING_WIN_START_DATE=$(date -d "$DATE_TODAY -45 days" +%Y-%m-%d)
echo "$ROLLING_WIN_START_DATE"
ROLLING_WIN_END_DATE=$(date -d "$DATE_TODAY" +%Y-%m-%d)
echo "$ROLLING_WIN_END_DATE"

## Generating the file suffixes
CUREENT_MONTH=$(date -d "$DATE_TODAY" +%Y-%m)
echo "$CUREENT_MONTH"
PREV_MONTH=$(date -d "$DATE_TODAY - 1 months" +%Y-%m)
echo "$PREV_MONTH"

## Defining an array to store the names of the file corresponding to current and previous month!
declare -A filename_array
filename_array[0]=yellow_tripdata_$PREV_MONTH 
filename_array[1]=yellow_tripdata_$CUREENT_MONTH
echo "here is the array"
echo "${filename_array[@]}"

## Running the python file
python ${ROOT_PATH_CODE}\\python\\RollingAverage.py \
    --list "$NYC_DATA_PATH" \
           "${filename_array[@]}" \
           "$ROLLING_WIN_START_DATE" \
           "$ROLLING_WIN_END_DATE" \
           "$OUTPUTPATH"









